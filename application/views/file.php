<html>
<body>
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Filename</th>
                <th>Date Added</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($files as $file): ?>
                <tr>
                    <td><?= $file->title ?></td>
                    <td><?= $file->thumbnail ?></td>
                    <td><?= $file->filename ?></td>
                    <td><?= $file->date_added ?></td>
                </tr>
            <?php endforeach; ?> 
        </tbody>
    </table>
    <?php
        echo Form::open(NULL, array('enctype' => 'multipart/form-data','id' => 'form'));
        echo Form::label('title', 'Title');
        echo Form::input('title');
        echo Form::label('image', 'Image');
        echo Form::file('image');
        echo Form::button('save', 'Save', array('type' => 'submit', 'class' => 'testAjax'));
        echo Form::close()
    ?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

$(document).ready(function() {
    $('#form').on('submit', function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
        url: '/index.php/welcome/store',
        data: data,
        processData: false,
        type: 'POST',
        success: function ( data ) {
            alert( data );
        }
    });

    })
});
</script>
</html>
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

	public function action_index()
	{
		$files = ORM::factory('File')->find_all();
		$view = View::factory('file')
			->bind('files', $files);
		$this->response->body($view);
	}

	public function action_store(){
		// $this->auto_render = false;
		// echo dd($this->request->post());

		$files = ORM::factory('File');
		$upload = Upload::save($_FILES['image']);
		// Resize, sharpen, and save the image
		$image = Image::factory($upload)->resize(200, NULL);
		$image->save();
		$thumbnail_path = dirname($image->file).'/thumb_'.basename($image->file);
		$thumbnail = Image::factory($upload)->resize(100, NULL); 
		$thumbnail->save($thumb_path);

		$files->title = $_POST['title'];
		$files->thumbnail = $_POST['title'];
		$files->filename = $_POST['title'];
		$files->date_added = date('Y-m-d H:i:s');

		$files->save();

		echo json_encode(array($files));
	}

} // End Welcome
